//======================================================================
// cat.h
//======================================================================

#ifndef __iop_cat_h__
#define __iop_cat_h__

//#define ACK 0
//#define CAT_PREFIX 0xC0
//#define IOP_PREFIX 0xD0
//#define EEPROM_READ_PREFIX 0xE0
//#define EEPROM_WRITE_PREFIX 0xF0

#include <iopcomm.h>
#include "config.h"

#define USBSERIAL Serial
#define HWSERIAL Serial1

void initCAT(long, int);
void serviceCAT();

#endif

//======================================================================
// EOF
//======================================================================
