//======================================================================
// TxSwitch.h
//======================================================================

#ifndef __TxSwitch_h__
#define __TxSwitch_h__

#include <Bounce2.h>
#define BOUNCE_WITH_PROMPT_DETECTION

#include "ubitx_iop.h"
#include "rig.h"

#define MIC_PTT_PIN 21
#define LINE_PTT_PIN 20

//----------------------------------------------------------------------
// ITxSwitch
// 
// Interface for transmit (PTT, Key) switches.  onPress() is called
// before transmission begins, and should return true if transmission
// should start.  onRelease() is called before transmission ends, and
// should return true if transmission should in fact end.
//----------------------------------------------------------------------

class ITxSwitch
{
  public:
  virtual ~ITxSwitch() {}

  // Called before beginning transmit; if false, transmit aborts before starting.
  virtual bool onPress(basic_mode* m) = 0; 

  // Called before stopping tranmit; if false, transmit continues.
  virtual bool onRelease(basic_mode* m) = 0;

  void press(basic_mode* m, bool output_enable=true) {
    if (onPress(m)) {
      USBDEBUG("PTT pressed");
      m->tx();
      if (output_enable) {
        setKeyDown(); // NOTE: could still make this more configurable...
      }
    }
  }

  void release(basic_mode* m, bool output_enable=true) {
    if (onRelease(m)) {
      USBDEBUG("PTT released");
      if (output_enable) {
        setKeyUp(); // NOTE: could still make this more configurable...
      }
      m->rx();
    }
  }
};

//----------------------------------------------------------------------
// CATSwitch
//
// Implementation of the ITxSwitch interface for the CAT control.  In
// general, CAT cannot override any existing transmission, and cannot
// terminate an existing transmission.
//----------------------------------------------------------------------

class CATSwitch : public ITxSwitch
{
  public:
  CATSwitch(): _transmitting(false) {}
  
  virtual bool onPress(basic_mode* m) {
    // If another transmission is already occuring, abort... CAT can't
    // interrupt transmissions already ongoing.
    if (m->is_rx()) {
      USBDEBUG("CAT PTT pressed");
      _transmitting = true;
      return true;
    } else {
      return false;
    }
  }
  
  virtual bool onRelease(basic_mode* m) {
    // If CAT transmission is not occurring, abort... CAT can't stop
    // transmissions initiated by other sources.  We don't check if 
    // the mode is already transmitting, because it could be 
    // transmitting because of CAT.
    if (_transmitting) {
      USBDEBUG("CAT PTT released");
      _transmitting = false;
      return true;
    } else {
      return false;
    }
  }

  private:
  bool _transmitting;  // CAT-specific transmission
};

//----------------------------------------------------------------------
// GPIOSwitch
//
// Class used to implement the physical transmit switches (i.e. the 
// Mic and Line input PTTs which are connected to GPIO pins).  Takes
// an object implementing the ITxSwitch interface, as well as info for
// setting up a debounced pin.
//----------------------------------------------------------------------

class GPIOSwitch : public ITxSwitch
{
  public:
  GPIOSwitch(bool is_mic, int pin, int msec=25): _is_mic(is_mic), _ssb_mode(false), _bounce() {
    _bounce.attach(pin, INPUT_PULLUP);
    _bounce.interval(msec);
  }

  inline void setSSBMode(bool flag) { _ssb_mode = flag; }
  
  virtual bool onPress(basic_mode* m) {
    if (m->is_rx()) {
      if (_ssb_mode) {
        if (_is_mic) {
          USBDEBUG("Mic PTT pressed");
          ((ssb_mode*)m)->set_mic_in();
        } else {
          USBDEBUG("Line PTT pressed");
          ((ssb_mode*)m)->set_line_in();
        }
      }
      return true;
    } else {
      return false;
    }
  }
  
  virtual bool onRelease(basic_mode* m) { 
    if (m->is_tx()) {
      return true;
    } else {
      return false;
    }
  }
  
  void update(basic_mode* m, bool output_enable=true) {
    _bounce.update();

    if (_bounce.fell()) {
      press(m, output_enable);
    } else if (_bounce.rose()) {
      release(m, output_enable);
    }
  }

  private:
  bool _is_mic;
  bool _ssb_mode;
  Bounce _bounce;
};

#endif

//======================================================================
// EOF
//======================================================================
