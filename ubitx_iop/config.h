//======================================================================
// config.h
//
// NOTE:  Let's change the name of this file to RigConfig.h.
//======================================================================

#ifndef __iop_config_h__
#define __iop_config_h__

#include <iopcomm.h>
//#include "rig.h"

#define KEYER_LEFT_PADDLE_PIN 16
#define KEYER_RIGHT_PADDLE_PIN 17

// Uncomment to use the "factory" calibration mode.  This is intended to
// allow calibration of IOP settings, separately from the uBITX/Raduino.
// There will be no pass-thru of any CAT.
//#define FACTORY_CALIBRATION

//======================================================================
// AudioConfig
//======================================================================

class AudioConfig {
  public:

  //--------------------------------------------------------------------
  // RECEIVE PARAMETERS
  //--------------------------------------------------------------------

  // rig-in parameters (RX)
  uint8_t rxRigInLevel   = 8; //5;
  float   rxRigInVol     = 1.0; //0.7;
  float   rxRigInCal     = 8.0; //1.0;
  // USB-in parameters (RX) - debug/monitor use only
  bool    rxUSBInEnable  = false;
  float   rxUSBInVol     = 1.0;
  float   rxUSBInCal     = 1.0;
  // speaker-out (DAC) parameters (RX)
  float   rxSpkrOutCal   = 1.0;
  // line-out parameters (RX)
  uint8_t rxLineOutLevel = 29;
  float   rxLineOutCal   = 1.0;
  // USB-out parameters (RX)
  float   rxUSBOutCal    = 1.0;
  // sidetone parameters (really a TX thing, but "RX" side of audio)
  float   sideToneVol    = 1.0;

  //--------------------------------------------------------------------
  // TRANSMIT PARAMETERS
  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  // NOTE: Default rig-out parameters are based on trying to hit a 
  // 25mVrms output to the rig.  This is based on some discussion at the
  // following URL:
  //
  // https://groups.io/g/BITX20/message/58951
  // 
  // This may or may not be totally applicable.  I believe it was for 
  // IMD/spurs on a pre-V5 version of the uBITX.  So it may be OBE for 
  // my V5.  It also alludes to modifications to the transmit audio 
  // chain, which may be something I need to look into (i.e. increasing
  // the gain on the initial mic pre-amp).
  //
  // Specifically, for the rig-out parameters:
  //    line out level 31 = 1.16 V p-p
  //                      = 0.58 V peak
  //                      = 0.41 V rms = 410 mV rms
  //    so output calibration needs to reduce that to 17.5 mV rms (70% 
  //    of 25 mV rms)  
  //           txMicInCal = 0.043 ... seems pretty low...
  //
  // Likewise, default line-in (TX) parameters assume consumer line 
  // level input, but with the default audio adaptor line-in level 
  // setting (5 = 1.33V p-p).
  //    line in level 5 = 1.33 V p-p
  //          signal in = 0.894 V p-p
  //                    = 0.447 V peak
  //                    = 0.316 V rms = 316 mV rms
  //    so input calibration needs to convert that to the 410 mV noted 
  //    above (equivalent for the microphone), so that when that signal 
  //    gets to the rig-out, it gets treated the same as the mic input.
  //        txLineInCal = 1.30
  //--------------------------------------------------------------------

  // microphone-in parameters (TX)
  uint8_t txMicInGain    = 12;
  float   txMicInVol     = 1.0; //0.7;
  float   txMicInCal     = 1.0;
  // line-in parameters (TX)
  uint8_t txLineInLevel  = 5;
  float   txLineInVol    = 1.0; //0.7;
  float   txLineInCal    = 1.30;
  // USB-in parameters (TX)
  float   txUSBInVol     = 1.0; //0.7;
  float   txUSBInCal     = 1.0;
  // two-tone parameters (TX)
  float   txSine1Vol     = 1.0;
  float   txSine2Vol     = 1.0;
  // rig-out parameters (TX) - default settings are based on hitting 70% of 25mVrms w/ mic
  uint8_t txRigOutLevel  = 31;
  float   txRigOutCal    = 0.1;   // 0.061;
  // USB-out parameters (TX)- debug/monitor use only
  bool    txUSBOutEnable = true;
  float   txUSBOutCal    = 1.0; 
};

//======================================================================
// RigConfig
// Used to store configuration parameters during runtime, as well as to
// save them to EEPROM.
//======================================================================

class RigConfig {
  public:
  RigConfig() {}
  // audio configuration
  AudioConfig audio;
  
  // mode configuration
  ssb_config  ssb;
  digi_config digi;
  cw_config   cw;

  // General rig configuration entries.
  rig_mode mode = rig_mode::ssb;
};

extern RigConfig rigConfig;

#endif

//======================================================================
// EOF
//======================================================================
