//======================================================================
// menu.ino
//======================================================================

#include <iopcomm.h>
#include "config.h"
#include "menu.h"

void do_nothing_func() {}
Update_func do_nothing = Update_func::create<do_nothing_func>();

List_menu<7> audio_config_menu_("Audio Config", {
  new Parm_uint8 ("RX ADC Lvl",  rigConfig.audio.rxRigInLevel,       0,   15,   1, do_nothing),
  new Parm_uint8 ("RX DAC Lvl",  rigConfig.audio.rxLineOutLevel,    13,   31,   1, do_nothing),
  new Parm_float ("RX Inp Lvl",  rigConfig.audio.rxRigInVol,       0.0,  1.0, 0.1, do_nothing),
  new Parm_float ("RX Inp Cal",  rigConfig.audio.rxRigInCal,       0.0, 99.9, 0.1, do_nothing),
  new Parm_float ("RX Spkr Cal", rigConfig.audio.rxSpkrOutCal,     0.0, 99.9, 0.1, do_nothing),
  new Parm_float ("RX Line Cal", rigConfig.audio.rxLineOutCal,     0.0, 99.9, 0.1, do_nothing),
  new Parm_float ("RX USB Cal",  rigConfig.audio.rxUSBOutCal,      0.0, 99.9, 0.1, do_nothing),
});

const Menu_item* audio_config_menu = &audio_config_menu_;

/* commented out til I go back and fix SSBConfig class
List_menu<3> ssb_config_menu("SSB Config", {
  Parm_float("Comp Thresh", rigConfig.lsb.comp_threshold, 0.0,  1.0, 0.1, do_nothing),
  Parm_float("Comp Ratio",  rigConfig.lsb.comp_ratio,     0.0, 50.0, 1.0, do_nothing),
  Parm_float("Comp Gain",   rigConfig.lsb.comp_gain,      0.0, 99.9, 0.1, do_nothing)
});
*/

/*
ListMenu("Configuration",
  ListMenu("LSB Mode",
    ParameterFilter("Filter",      c.lsb.filter),
    ParameterOnOff ("Compressor",  c.lsb.comp_enable),
    ParameterFloat ("Comp Thresh", c.lsb.comp_threshold,   0.0,  1.0, 0.1),
    ParameterFloat ("Comp Ratio",  c.lsb.comp_ratio,       0.0, 50.0, 1.0),
    ParameterFloat ("Comp Gain",   c.lsb.comp_gain,        0.0, 99.9, 0.1)),
  ListMenu("USB Mode",
    ParameterFilter("Filter",      c.usb.filter),
    ParameterOnOff ("Compressor",  c.usb.comp_enable),
    ParameterFloat ("Comp Thresh", c.usb.comp_threshold,   0.0,  1.0, 0.1),
    ParameterFloat ("Comp Ratio",  c.usb.comp_ratio,       0.0, 50.0, 1.0),
    ParameterFloat ("Comp Gain",   c.usb.comp_gain,        0.0, 99.9, 0.1)),
  ListMenu("CWL Mode",
    ParameterFilter("Filter",      c.cwl.filter),
    ParameterKeyer ("Keyer Mode",  c.cwl.mode),
    ParameterOnOff ("Keyer Rev?",  c.cwl.reversed),
    ParameterInt   ("Keyer WPM",   c.cwl.wpm,                0,   60,   1),
    ParameterFloat ("Keyer Weight", c.cwl.weight,          0.0,  9.9, 0.1),
    ParameterInt   ("ST Freq",     c.sidetone,             300, 3000,  10)),
  ListMenu("CWU Mode",
    ParameterFilter("Filter",      c.cwl.filter),
    ParameterKeyer ("Keyer Mode",  c.cwl.mode),
    ParameterOnOff ("Keyer Rev?",  c.cwl.reversed),
    ParameterInt   ("Keyer WPM",   c.cwl.wpm,                0,   60,   1),
    ParameterFloat ("Keyer Weight", c.cwl.weight,          0.0,  9.9, 0.1),
    ParameterInt   ("ST Freq",     c.sidetone,             300, 3000,  10)),  
  ListMenu("DGL Mode",
    ParameterFilter("Filter",      c.dgl.filter)),
  ListMenu("DGU Mode",
    ParameterFilter("Filter",      c.dgu.filter)),
  ListMenu("TTL Mode",
    ParameterFilter("Filter",      c.dgl.filter)),
  ListMenu("TTU Mode",
    ParameterFilter("Filter",      c.dgu.filter)),
  ListMenu("Audio",
    ParameterInt   ("RX ADC Lvl",  c.rxRigInLevel,       0,   15,   1),
    ParameterInt   ("RX DAC Lvl",  c.rxLineOutLevel,    13,   31,   1),
    ParameterFloat ("RX Inp Lvl",  c.rxRigInVol,       0.0,  1.0, 0.1),
    ParameterFloat ("RX Inp Cal",  c.rxRigInCal,       0.0, 99.9, 0.1),
    ParameterFloat ("RX Spkr Cal", c.rxSpkrOutCal,     0.0, 99.9, 0.1),
    ParameterFloat ("RX Line Cal", c.rxLineOutCal,     0.0, 99.9, 0.1),
    ParameterFloat ("RX USB Cal",  c.rxUSBOutCal,      0.0, 99.9, 0.1),
    ParameterOnOff ("RX USB Dbg",  c.rxUSBInEnable),
    ParameterInt   ("TX Mic Gain", c.txMicInGain,        0,   63,   1),
    ParameterInt   ("TX ADC Lvl",  c.txLineInLevel,      0,   15,   1),
    ParameterInt   ("TX DAC Lvl",  c.txRigOutLevel,     13,   31,   1),
    ParameterFloat ("TX Out Cal",  c.txRigOutCal,      0.0, 99.9, 0.1),
    ParameterFloat ("TX Mic Lvl",  c.txMicInVol,       0.0,  1.0, 0.1),
    ParameterFloat ("TX Mic Cal",  c.txMicInCal,       0.0, 99.9, 0.1),
    ParameterFloat ("TX Line Lvl", c.txLineInVol,      0.0,  1.0, 0.1),
    ParameterFloat ("TX Line Cal", c.txLineInCal,      0.0, 99.9, 0.1),
    ParameterFloat ("TX USB Lvl",  c.txUSBInVol,       0.0,  1.0, 0.1),
    ParameterFloat ("TX USB Cal",  c.txUSBInCal,       0.0, 99.9, 0.1),
    ParameterOnOff ("TX USB Dbg",  c.txUSBOutEnable),
    ParameterFloat ("Tone 1 Lvl",  c.txSine1Vol,       0.0,  1.0, 0.1),
    ParameterFloat ("Tone 2 Lvl",  c.txSine2Vol,       0.0,  1.0, 0.1),
    ParameterFloat ("ST Lvl",      c.sideToneVol,      0.0,  1.0, 0.1)));
*/

//======================================================================
// EOF
//======================================================================
